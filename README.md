# λ-Calculus Interpreter [![License MIT][badge-license]](LICENSE.txt)
A simple lambda calculus interpreter

## 2 Modes:
#### 1) Give lambda expressions:
1. λ : \\
2. e.g. "\\x.\\y.x"
```
> reduceNF (myparse "(\\x.\\y.zxy)w")
["(\\x.\\y.zxy)w", "\\b.zwb", "zw"]

> reduceNF (myparse "(\\x.\\y.zxy)(wy)")
["(\\x.\\y.zxy)(wy)", "\\b.z(wy)b", "z(wy)"]

> reduceNF (myparse "(\\n.\\f.\\x.nf(fx))(\\f.\\x.fx)")
["(\\n.\\f.\\x.nf(fx))(\\f.\\x.fx)", "\\b.\\c.(\\f.\\x.fx)b(bc)", "\\b.\\c.(\\c.bc)(bc)", "\\b.\\c.b(bc)"]
```
#### 2) Give fixed terms (true, false, chuch 2, etc):
1. true       : chTrue
2. false      : chFalse
3. ifthenelse : chCond
4. church num : church
5. succ       : chSucc
6. plus       : chPlus
7. mult       : chMult
8. exp        : chExp
9. iszero     : chIsZero
10. pair      : chPair
11. fst       : chFst
12. snd       : chSnd
13. and       : chAnd
14. or        : chOr
```
> prettyprint (chSucc (church 2))
"\\f.\\x.f(f(fx))"

> prettyprint (chIsZero (church 0))
"\\x.\\y.x"

> prettyprint (chPlus (chSucc (church 2)) (church 3))
"\\f.\\x.f(f(f(f(f(fx)))))"

> prettyprint (chMult (church 2) (church 3))
"\\f.\\b.f(f(f(f(f(fb)))))"

> prettyprint (chExp (church 2) (church 3))
"\\b.\\c.b(b(b(b(b(b(b(bc)))))))"

> prettyprint (chNot chFalse)
"\\x.\\y.x"

> prettyprint (chSnd (chPair (church 2) (church 3)))
"\\f.\\x.f(f(fx))"

> prettyprint (chOr chFalse  chTrue)
"\\x.\\y.x"
```

2015-2016


[badge-license]: https://img.shields.io/badge/license-MIT-green.svg?style=flat-square

# (Alonzo) Church's Numeral Interpretation

### Muhammad Dahlan Yasadipura - 1806205193 - Functional Programming 2020

Original Source Code oleh [Dimitris Mouris](https://github.com/jimouris/lambda-calculus-interpreter)

### Fitur yang selesai 

```
fitur number to churcNumeral

PS C:\Users\Dahlan\Desktop\PemFung\Tugas5\tugas5> ./lci
Type a "lambda" or " " for digit Operator or ^D to exit:
" "
> Type a digit operation like "+ 2 5" or "* 2 5" or "5" or ^D to exit:
"6"
> Normal form of 6:
\f.\x.f(f(f(f(f(fx)))))
In Integer : 6

PS C:\Users\Dahlan\Desktop\PemFung\Tugas5\tugas5> ./lci
Type a "lambda" or " " for digit Operator or ^D to exit:
" "
> Type a digit operation like "+ 2 5" or "* 2 5" or "5" or ^D to exit:
"5"
> Normal form of 5:
\f.\x.f(f(f(f(fx))))
In Integer : 5
```

```
fitur operator + dan *

PS C:\Users\Dahlan\Desktop\PemFung\Tugas5\tugas5> ./lci
Type a "lambda" or " " for digit Operator or ^D to exit:
" "
> Type a digit operation like "+ 2 5" or "* 2 5" or "5" or ^D to exit:
"* 2 5"
> Normal form of * 2 5:
\f.\b.f(f(f(f(f(f(f(f(f(fb)))))))))
In Integer : 10

PS C:\Users\Dahlan\Desktop\PemFung\Tugas5\tugas5> ./lci
Type a "lambda" or " " for digit Operator or ^D to exit:
" "
> Type a digit operation like "+ 2 5" or "* 2 5" or "5" or ^D to exit:
"+ 5 5"
> Normal form of + 5 5:
\f.\x.f(f(f(f(f(f(f(f(f(fx)))))))))
In Integer : 10
```

```
fitur konversi lambda calculus

PS C:\Users\Dahlan\Desktop\PemFung\Tugas5\tugas5> ./lci
Type a "lambda" or " " for digit Operator or ^D to exit:
"lambda"
> Type a lambda expression like "(\\x.\\y.x)" or ^D to exit:
"\\b.\\c.b(b(b(b(b(b(b(bc)))))))"
> Normal form of \b.\c.b(b(b(b(b(b(b(bc))))))):
\b.\c.b(b(b(b(b(b(b(bc)))))))
In Integer : 8

Type a "lambda" or " " for digit Operator or ^D to exit:
"lambda"
> Type a lambda expression like "(\\x.\\y.x)" or ^D to exit:
"(\\y.zy(\\x.xy))w"
> Normal form of (\y.zy(\x.xy))w:
In Integer : -1
not integer
```

## Cara Menjalankan Program
1. ghc -o lci lci.hs
2. ./lci
3. "lambda" untuk menjalankan lambda conversion atau " " untuk menjalankan operator

## kode yang diubah

* diubah agar terdapat 2 jalur dengan if condition
```
main :: IO ()
main = do  
    putStrLn ("Type a \"lambda\" or \" \" for digit Operator or ^D to exit:")
    putStr "> "
    inputStr <- readLn
    if inputStr == "lambda" then loopPrinter
    else digitPrinter
```

##### Kode yang ditambahkan

*membuat do baru agar dapat dipisah pengejaannya

```
digitPrinter = do
    putStrLn("Type a digit operation like \"+ 2 5\" or \"* 2 5\" or \"5\" or ^D to exit:")
    putStr "> "
    inputStr <- readLn
    let parsedString = split (inputStr)
    putStrLn ("Normal form of " ++ inputStr ++ ": ")
    let result = operateOrNumber parsedString
    putStrLn(prettyprint (result))
    putStrLn ("In Integer : " ++ show ( churchToInt ((reduceNF result)!!((length (reduceNF result))-1))))
    putStrLn (" ")

```

*beberapa fungsi baru untuk memudahkan pengolahan input
```
split :: String -> [String]
split [] = [""]
split (c:cs) | c == ' '  = "" : rest
             | otherwise = (c : head rest) : tail rest
    where rest = split cs

digitOperateParse :: [String] -> Term
digitOperateParse str 
    | str!!0 == "*" = chMult(church (read (str!!1) :: Integer))(church (read (str!!2) :: Integer))
    | str!!0 == "+" = chPlus(church (read (str!!1) :: Integer))(church (read (str!!2) :: Integer))

operateOrNumber :: [String] -> Term
operateOrNumber lst 
    |length lst == 1 = church (read (lst!!0) :: Integer)
    |otherwise = digitOperateParse lst

```